import {Canvas} from './Canvas.js';
import {Item} from './Item.js';
import {Move} from './Move.js';
import {MovingUp} from './MovingUp.js';
import {MovingDown} from './MovingDown.js';
import {MovingLeft} from './MovingLeft.js';
import {MovingRight} from './MovingRight.js';
import {Snake} from './Snake.js';
export class Game{
  private canvas:Canvas;
  private snake:Snake;
  private item:Item;
  private intervalID:number;
  private stopped:boolean;
  private moveSnake:Move;
  private readKeyboard:boolean; // Prevents multiple inputs in one frame
  constructor(){
    this.canvas=new Canvas();
    this.snake=new Snake(10,10,this.canvas);
    this.item=new Item(5,5,this.canvas);
    window.addEventListener("keydown", () =>this.keypress(event,this.snake));
    this.moveSnake = new MovingRight();
    this.readKeyboard = true;
  }
  public play(){
    this.intervalID=window.setInterval(()=>this.frame(),100);
    this.stopped=false;
  }
  public stop(){
    this.stopped=true;
    window.clearInterval(this.intervalID);
  }
  public toggle():void{
    if(this.stopped){
      this.play();
    }else{
      this.stop();
    }
  }
  private keypress(e:any,snake:Snake){
    if(this.readKeyboard){
      switch (e.keyCode){
        case 87:
          if(this.moveSnake.up(snake)){
            this.setMove(new MovingUp());
          }
          break;
        case 83:
          if(this.moveSnake.down(snake)){
            this.setMove(new MovingDown());
          }
          break;
        case 68:
          if(this.moveSnake.right(snake)){
            this.setMove(new MovingRight());
          }
          break;
        case 65:
          if(this.moveSnake.left(snake)){
            this.setMove(new MovingLeft());
          }
          break;
        case 32:
          this.toggle();
          break;
      }
      this.readKeyboard = false;
    }
  }
  private frame(){
    this.canvas.clear();
    this.dibujaTablero();
    this.dibujaBordes('red');
    this.item.draw();
    this.snake.step();
    if(this.snake.isDead(1,18,1,18)){
      this.gameOver();
    }
    this.snake.checkItem(this.item);
    this.snake.draw();
    this.readKeyboard = true;
  }
  private dibujaTablero(){
    for(let fila:number=0;fila<20;fila++){
      for(let columna:number=0;columna<20;columna++){
        this.dibujaCasilla(fila,columna,'blue');
      }
    }
  }
  private dibujaBordes(color:string){
    this.dibujaFila(0,color);
    this.dibujaFila(19,color);
    this.dibujaColumna(0,color);
    this.dibujaColumna(19,color);
  }
  private dibujaFila(fila:number, color:string){
    for(let columna:number=0;columna<20;columna++){
      this.dibujaCasilla(fila,columna,color);
    }
  }
  private dibujaColumna(columna:number, color:string){
    for(let fila:number=0;fila<20;fila++){
      this.dibujaCasilla(fila,columna,color);
    }
  }
  private dibujaCasilla(fila:number,columna:number,color:string){
    this.canvas.draw(fila*20+5,columna*20+5,10,10,color);
  }
  private gameOver(){
    this.stop();
    console.log('Game Over');
  }
  public setMove(m:Move){
    this.moveSnake = m;
  }
}

let g = new Game();
g.play();
