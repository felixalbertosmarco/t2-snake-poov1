import {Game} from './Game.js';
import {Snake} from './Snake.js';
export class Move{
  public constructor(){
  }
  public up(snake:Snake):boolean{
    snake.up();
    return true;
  }
  public down(snake:Snake):boolean{
    snake.down();
    return true;
  }
  public right(snake:Snake):boolean{
    snake.right();
    return true;
  }
  public left(snake:Snake):boolean{
    snake.left();
    return true;
  }
}
