export class Canvas{
  private canvas:HTMLCanvasElement;
  private ctx:CanvasRenderingContext2D;
  public constructor(){
    this.canvas = document.createElement('canvas') as HTMLCanvasElement;
    this.canvas.width=400;
    this.canvas.height=400;
    document.body.appendChild(this.canvas);
    this.ctx = this.canvas.getContext('2d');
    this.clear();
  }
  public draw(x:number,y:number,w:number,h:number,c:string){
    this.ctx.fillStyle = c;
    this.ctx.fillRect(x,y,w,h);
  }
  public clear(){
    this.draw(0,0,this.canvas.width,this.canvas.height,'black');
  }
}
